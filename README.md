# ArangoDB Manager

This repository contains script used to manage an ArangoDB instance. This is mainly used to automatically create the collections needed for the Adventurer Service.

# Usage
Download the last release [here](https://gitlab.com/knowledge-conquest/arangodb-manager/-/releases/1.0)

Create a file name `config.properties` right by the downloaded file.

Here are all the properties that can be modifier and their defaut values:
```
host=localhost
port=8529
root=root
rootPassword=secret
user=admin
userPassword=admin
dbName=arango
```
You can copy this and modify it to match you setup.

Then run this command to setup the database:
```
java -jar <path-to-downloaded-jar> <path-to-configfile>
```
or (as an example)
```
java -jar kc-arangodb-manager-1.0.jar config.properties
```
```