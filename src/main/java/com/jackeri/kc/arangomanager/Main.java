import com.arangodb.ArangoDatabase;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");
        setupAdventurerServiceDB();
    }

    public static void setupAdventurerServiceDB() {
        ArangoConfig config = new ArangoConfig();
        ArangoService service = new ArangoService(config);

        service.setupDatabase();
        service.shutdown();
    }
}
